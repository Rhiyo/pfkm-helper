# Pathfinder Kingmaker Helper

Liver version found at: https://rhiyo.gitlab.io/pfkm-helper/

A webapp for helping with various Pathfinder Kingmaker quests. As of now it only helps with the Magic Prison quest by listing all creatures that you have yet to slay.

Mostly developed in JavaScript. This project was made before the Definitive Edition and as such may not be accurate. However, from what people have said online it seems to mostly work. Some people have said it has reported monsters that they have already slayed, though.

History files for testing can be found [here](https://drive.google.com/drive/folders/1d5zXmKlmBPggUS7FD963p2KPUGjpuz9H).

## Running yourself

This is mostly kept on a public repository to make use of pages. However, if you do want to run this yourself you'll have to have npm and run
```
npm install
```
to install the required javascript packages. Then you can just open index.html and use it locally, however you may have issues with [CORS](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS).

## TODO

- I'd like to implement a testing a structure but as of now it is hard as most of the code is hap-hazardly bundled into one function. Within the testing stage I'd also like to add a linter.
- Annotate the test history files with what they actually have to perform proper testing.
- The csv file has information about all the artifacts and fragments too, and I've started to code for scraping what a user has found in their history file. The main issue is that it lacks any UI.
- A map to visually show what the user is missing and where it is.

Can find blueprint IDs at:
https://github.com/spacehamster/KingmakerDataminer/releases/tag/blueprints
