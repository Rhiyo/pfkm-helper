let dropArea = document.getElementById('drop-area');

const CREATURES_NEEDED = 45

dropArea.addEventListener('dragenter', highlight, false)
dropArea.addEventListener('dragleave', unhighlight, false)
dropArea.addEventListener('dragover', highlight, false)
dropArea.addEventListener('drop', unhighlight, false)

    ;['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
        dropArea.addEventListener(eventName, preventDefaults, false)
    })

function preventDefaults(e) {
    e.preventDefault()
    e.stopPropagation()
}

function highlight(e) {
    dropArea.classList.add('highlight')
}

function unhighlight(e) {
    dropArea.classList.remove('highlight')
}

dropArea.addEventListener('drop', handleDrop, false)

document.getElementById('file-input').addEventListener('change', function (e) {
    const file = e.target.files[0];
    if (file) processFile(file);
});

// Handle the file drop
function handleDrop(e) {
    e.preventDefault();
    let dt = e.dataTransfer;
    let files = dt.files;
    processFile(files[0]);
}

async function loadCSVData() {
    const csv_data = await d3.csv("PFData.csv", function (d) {
        return {
            region: d.Region,
            map: d.Map,
            requirement: d.Requirement,
            flag: d.Flag,
            AreaFlag: d.AreaFlag,
            AreaBluePrint: d.AreaBluePrint,
            UnlockableFlag: d.UnlockableFlag,
            LairTrigger: d.LairTrigger,
            for: d.For,
            playerFound: false,
            historyFound: false,
            zksFound: false
        };
    });
    return csv_data;
}

async function getMagicPrisonData() {
    const csv_data = await loadCSVData(); // Await the Promise to get the array
    const filtered_data = csv_data.filter(row => row.for === "MP"); // Now you can filter the array
    return filtered_data;
}

// Function to process the file
async function processFile(file) {
    let mp_data = await getMagicPrisonData();
    let results_data;
    document.getElementById('result').innerHTML = "Reading file...";
    if (file.name === "history") {
        // Read the direct history file as a string using FileReader
        results_data = await new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.onload = () => resolve(manageHistoryFile(reader.result, mp_data));
            reader.onerror = () => reject("Error reading history file.");
            reader.readAsText(file);
        });
    } else if (file.name.endsWith(".zks")) {
        results_data = await processZKSSave(file, mp_data);
    } else if (file.name === "player.json") {
        results_data = await managePlayerFile(file, mp_data);
    } else {
        document.getElementById('result').innerHTML = "File unreadable";
        return;
    }
    displayResults(results_data)
}

async function processZKSSave(file, mp_data) {
    const jszip = new JSZip();
    const zip = await jszip.loadAsync(file);

    const results = {};
    mp_data.forEach(row => {
        if (row.LairTrigger && row.LairTrigger.trim() !== "") {
            const fileName = `${row.AreaBluePrint}.json`;
            results[fileName] = results[fileName] || [];
            results[fileName].push(row);
        }
    });

    // Iterate through each file in the zip that matches areaBlueprints
    for (const zipEntryName of Object.keys(results)) {
        const zipEntry = zip.file(zipEntryName);
        if (!zipEntry) continue;

        const content = await zipEntry.async("string"); // Read file content as JSON text
        const areaData = JSON.parse(content); // Parse JSON content

        // Check if areaData has m_ComponentsData with matching ComponentName in each row
        const componentsData = areaData?.m_Area?.m_ComponentsData || [];
        results[zipEntryName].forEach(row => {
            componentsData.forEach(component => {
                if (component.ComponentName === row.LairTrigger) {
                    row.zksFound = true; // Mark as found if ComponentName matches LairTrigger
                }
            });
        });
    }

    // Process player.json if it exists
    const player_file = zip.file('player.json');
    if (player_file) {
        const playerContent = await player_file.async("string"); // Get player file content as string
        mp_data = await managePlayerFile(playerContent, mp_data, true);
    }

    // Process history file if it exists
    const history_file = zip.file('history');
    if (history_file) {
        const historyContent = await history_file.async("string"); // Get history file content as string
        mp_data = manageHistoryFile(historyContent, mp_data); // Pass content directly
    }

    return mp_data;
}


function displayResults(data) {
    // Calculate missing count for each row and sort the data by most to least missing
    data.sort((a, b) => {
        const missingCountA = [!a.historyFound, !a.zksFound, !a.playerFound].filter(Boolean).length;
        const missingCountB = [!b.historyFound, !b.zksFound, !b.playerFound].filter(Boolean).length;
        return missingCountB - missingCountA;
    });

    // Start building the description and table with styles
    let showText = `
    <div style="display: flex; flex-direction: column; align-items: center; padding: 20px; max-width: 800px; margin: 0 auto; text-align: left;">
        <p><strong>History File:</strong> Checks your history file to see if you've met the requirements. The history file may not exist due to mods.</p>
        <p><strong>ZKS Area Files:</strong> Looks through all your area data to check for specific flags of clearing the lair. This may be unreliable and these flags may not exist if you are at House at the Edge of Time.</p>
        <p><strong>Player.json File:</strong> Looks at your player.json to see if you've visited the area before or set off a related flag.</p>
    </div>
    <table border="1" style="width: 80%; max-width: 800px; border-collapse: collapse; margin: 20px auto; text-align: center;">
        <caption style="font-weight: bold; padding: 10px; font-size: 1.2em;">
            Creature Checklist - Ordered by Missing Data
        </caption>
        <tr>
            <th title="Reliable when present, checks the creature in history logs">History File</th>
            <th title="Most accurate, checks if creature exists in ZKS data">Area Files</th>
            <th title="General presence, indicates if you've visited the area">Player.json File</th>
            <th>Requirement</th>
            <th>Region</th>
            <th>Map</th>
        </tr>
    `;

    // Function to generate a row for a given creature entry
    const generateRow = (row) => `
        <tr>
            <td style="color: ${row.historyFound ? 'green' : 'gray'}; font-weight: ${row.historyFound ? 'normal' : 'bold'}; background-color: ${row.historyFound ? 'transparent' : '#ffeeee'};">
                ${row.historyFound ? "True" : "Missing"}
            </td>
            <td style="color: ${row.zksFound ? 'green' : 'orange'}; font-weight: ${row.zksFound ? 'normal' : 'bold'}; background-color: ${row.zksFound ? 'transparent' : '#fffbdd'};">
                ${row.zksFound ? "True" : "Missing"}
            </td>
            <td style="color: ${row.playerFound ? 'green' : 'red'}; font-weight: ${row.playerFound ? 'normal' : 'bold'}; background-color: ${row.playerFound ? 'transparent' : '#ffe6e6'};">
                ${row.playerFound ? "True" : "Not Visited"}
            </td>
            <td>${row.requirement}</td>
            <td>${row.region}</td>
            <td>${row.map}</td>
        </tr>
    `;

    // Add rows in sorted order based on missing count
    data.forEach(row => {
        showText += generateRow(row);
    });

    showText += `
            </table>
        </div>
    `;

    // Display the table in the result element
    document.getElementById('result').scrollIntoView();
    document.getElementById('result').innerHTML = showText;
}

function managePlayerFile(fileContent, data, isZipContent = false) {
    return new Promise((resolve, reject) => {
        const processContent = (content) => {
            try {
                const playerData = JSON.parse(content);

                for (const obj of playerData.VisitedAreasData) {
                    for (const element of data) {
                        if (element.AreaBluePrint === obj.Key) {
                            if (element.UnlockableFlag !== "") {
                                for (const unlockObj of playerData.m_UnlockableFlags.m_UnlockedFlags) {
                                    if (element.UnlockableFlag === unlockObj.Key) {
                                        element.playerFound = 1;
                                        break;
                                    }
                                }
                            } else {
                                element.playerFound = 1;
                            }
                        }
                    }
                }
                resolve(data); // Resolve the promise with the modified data
            } catch (error) {
                reject(error); // Reject the promise if there's an error
            }
        };

        if (isZipContent) {
            // If the fileContent is from a zip, we assume it's already a string
            processContent(fileContent);
        } else {
            // If fileContent is a direct file, use FileReader to read it
            const reader = new FileReader();
            reader.onloadend = () => processContent(reader.result);
            reader.onerror = reject; // Reject if there's a read error
            reader.readAsText(fileContent);
        }
    });
}

function manageHistoryFile(file, data) {
    // Map to track flags by area
    const areaFlagMap = new Map();
    data
        .filter((e) => e.requirement)
        .forEach((element) => areaFlagMap.set(element.AreaFlag, element.flag));

    let flagsFound = new Set();
    let lairsClearedFound = false;
    let currentArea = "";

    // Split the file into lines
    const lines = file.split('\n');

    // Process each line in the file
    for (const oneLine of lines) {
        // Check if all rows have historyFound set to true
        if (data.every(row => row.historyFound)) {
            break; // Stop processing if all are found
        }

        const line = oneLine.split(" ");
        if (line.length < 7) continue;

        const lineCategory = line[3];
        const lineFlag = line[4];

        const areaLoading = lineCategory === "[Area]" && oneLine.includes(": loading area");
        // Set area
        if (areaLoading) {
            // Process the previous area
            flagFoundInArea(areaFlagMap, currentArea, lairsClearedFound, flagsFound, data);

            currentArea = lineFlag;
            flagsFound.clear();
            lairsClearedFound = false;
        }

        if (!areaFlagMap.has(currentArea)) continue;

        // Process interesting lines based on category
        if (lineCategory === "[Area]" && oneLine.includes(" unit dies")) {
            flagsFound.add(lineFlag);
        } else if (lineCategory === "[Unlocks]" && oneLine.includes(" unlocked")) {
            flagsFound.add(lineFlag);
        } else if (lineCategory === "[Unlocks]" && lineFlag === "LairsCleared") {
            lairsClearedFound = true;
        }
    }

    // Process the very last area
    flagFoundInArea(areaFlagMap, currentArea, lairsClearedFound, flagsFound, data);
    return data; // Return the updated data
}

function flagFoundInArea(areaFlagMap, area, lairsCleared, foundFlags, data) {
    const requiredFlag = areaFlagMap.get(area);
    console.log(foundFlags)
    if (requiredFlag && lairsCleared && foundFlags.has(requiredFlag)) {
        for (const element of data) {
            if (element.AreaFlag === area && element.flag === requiredFlag) {
                element.historyFound = true; // Update to historyFound
                areaFlagMap.delete(area);
                return true;
            }
        }
    }
    return false;
}