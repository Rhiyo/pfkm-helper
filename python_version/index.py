
#Load history file

#Have list of all creature names

#Split by space until Lairs CLeared is found as 5th element
#Remove 5th element of previous line from creature name list
# if 7 element is unlocked, skip next line

#print remaining creatures from list

from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
import pandas as pd
import sys
from datetime import datetime

def checkCreatures(lines):
    df = pd.read_csv("PFData.csv")
    df['Found'] = 0
    CREATURES_NEEDED = 45

    interval = 1
    line_length = len(lines)

    prevLine = None
    #unknownFlags = []
    foundCreatures = 0

    #Cache lists
    areas = df['AreaFlag'].unique()
    flags = df['Flag'].unique()

    #Need to store area incase of dupe flags
    currentArea = None

    now = datetime.now()
    for n, line in enumerate(lines):
        percent = (n+1)/float(line_length)
        if  percent > 0.05*interval:
            interval+=1
            print(str(percent*100)+"%")
        line = line.split(" ")
        if len(line) < 7:
            prevLine = line
            continue

        #Set area
        if line[3] == "[Area]" and line[6] == "loading":
            currentArea = line[4]

        #Nothing interesting happens in these areas
        if not currentArea in areas or (not line[4] in flags and not line[4] == "LairsCleared"):
            continue

        #Check status of magical prison creatures
        if prevLine != None:
            if line[4] == "LairsCleared" and prevLine[6] != "unlocked":
                #if prevLine[4] in df['Flag'].values:
                    #print("Hello")
                    #creatures.remove(prevLine[4]);
                    #if not (currentArea in df['AreaFlag'].values):
                    #    df.loc[df['Flag'] == prevLine[4],'Found'] = 1
                    #    foundCreatures
                    #else:
                df.loc[(df['Flag'] == prevLine[4]) & (df['AreaFlag'] == currentArea),'Found'] = 1
                foundCreatures += 1
                continue

        prevLine = line

        if(not line[6] == 'item'):
            continue

        line[7] = line[7].strip()

        #Check for artifacts
        fragments = {'Artifact_BanditAmulet-shard':[], 'Artifact_SoulsCloak-shard':[],'Artifact_HermitKnightBracers-shard':[],
               'Artifact_BraveryRing-shard':[],'Artifact_StarGauntlet-shard':[],'Artifact_TigerArmor-shard':[],
               'Artifact_TrailblazerHelmet-shard':[]}

        fragments_lost = {'Artifact_BanditAmulet-shard':{}, 'Artifact_SoulsCloak-shard':{},'Artifact_HermitKnightBracers-shard':{},
               'Artifact_BraveryRing-shard':{},'Artifact_StarGauntlet-shard':{},'Artifact_TigerArmor-shard':{},
               'Artifact_TrailblazerHelmet-shard':{}}

        if line[4] in fragments.keys():
            if line[7] == 'lost':
                if(currentArea in fragments_lost[line[4]].keys()):
                    fragments_lost[line[4]][currentArea] += 1
                else:
                    fragments_lost[line[4]][currentArea] = 1
            elif line[7] == 'found':
                if currentArea in fragments_lost[line[4]].keys():
                    if fragments_lost[line[4]][currentArea] > 0:
                         fragments_lost[line[4]][currentArea] -= 1
                    else:
                         df.loc[(df['Flag'] == line[4]) & (df['AreaFlag'] == currentArea), 'Found'] += 1
                         continue
                else:
                    df.loc[(df['Flag'] == line[4]) & (df['AreaFlag'] == currentArea), 'Found'] += 1
            continue

        #Only possible antique left
        df.loc[df['Flag'] == line[4], 'Found'] = 1

        if(df['Found'].sum() >= df['Occurences'].sum()):
            break

    for i, row in df[df['Found'] > df['Occurences']].iterrows():
        print(row['Flag'] + " from " + row['AreaFlag'] + " has " + str(row['Found']) + " occurences when it should have equal to or less than " +
                        str(row['Occurences']) + ".")

    #Generate the HTML to show what is needed
    showText = ""

    #Add into db
    #db.engine.execute("INSERT INTO flags (" + ", ".join(df['Flag']+"_"+df['AreaFlag']).replace("-","_") + ") VALUES(" +
    #            ", ".join(list(df['Occurences'].apply(str))) + ");").execution_options(autocommit=True)

    #last_id = db.engine.execute("SELECT max(id) FROM flags;").execution_options(autocommit=True)
    #
    #for flag in unknownFlags:
    #    db.engine.execute("INSERT INTO unknown_flags VALUES(" + last_id + ", " + flag + ");").execution_options(autocommit=True)

    if(foundCreatures < CREATURES_NEEDED):
        showText += "Remaining Creatures Needed Are: <br /><ul>"
        for _, creature in df[(df['For'] == "MP") & (df['Found'] == 0)].iterrows():
            showText = showText + "<li>" + creature['Requirement'] + " in " + creature['Map'] + ".</li>"
        showText += "</ul>"
    else:
        showText = "This history file shows that this save has already found all the required creatures."

    #print >> sys.stderr, (datetime.now() - now)
    return showText

#For future reference Antique_Tech_[1-5]_item are technic league items, 5 is in wicked hill
#1 is in second doomhydra dappled quagmire
#2 in werewolflairboss

app = Flask(__name__)
@app.route("/", methods=['GET','POST'])
def index():
    if request.method == "POST":
        return checkCreatures(request.get_json())
    return '''
<head>
<title>Pathfinder Kingmaker Helper</title>
<style>
    #location {
    border: 1px solid #708090;
    background-color: #708090;
    color: #D3D3D3;
    border-radius: 10px;
    }
    #drop-area {
    text-align: center;
    border: 2px dashed #ccc;
    border-radius: 20px;
    width: 480px;
    font-family: sans-serif;
    margin: 100px auto;
    padding: 20px;
    }
    #drop-area.highlight {
    border-color: purple;
    }
    p {
    margin-top: 0;
    }
    .my-form {
    margin-bottom: 10px;
    }
    #gallery {
    margin-top: 10px;
    }
    #gallery img {
    width: 150px;
    margin-bottom: 10px;
    margin-right: 10px;
    vertical-align: middle;
    }
    .button {
    display: inline-block;
    padding: 10px;
    background: #ccc;
    cursor: pointer;
    border-radius: 5px;
    border: 1px solid #ccc;
    }
    .button:hover {
    background: #ddd;
    }
    #fileElem {
    display: none;
    }

</style>

</head>
<body>
    <div id="drop-area">

            <p>This web app is for the game Pathfinder Kingmaker and is designed to help find missing monsters for the Magical Prison quest.<p>
            <p>Drop Your 'history' file in this box to see missing requirements for Magical Prison quest.</p>
            <p>The 'history' file of your last played save can be found in:<p>
            <div id="location">C:/users/<i>[Your Computer User Name Here]</i>/AppData/LocalLow/Owlcat Games/Pathfinder Kingmaker/Areas/</div>

    </div>

    <div id="result">

    </div>

    <script type="text/javascript">
        let dropArea = document.getElementById('drop-area');

        dropArea.addEventListener('dragenter', highlight, false)
        dropArea.addEventListener('dragleave', unhighlight, false)
        dropArea.addEventListener('dragover', highlight, false)
        dropArea.addEventListener('drop', unhighlight, false)

        ;['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
        dropArea.addEventListener(eventName, preventDefaults, false)
        })

        function preventDefaults (e) {
            e.preventDefault()
            e.stopPropagation()
        }

        function highlight(e) {
            dropArea.classList.add('highlight')
        }

        function unhighlight(e) {
            dropArea.classList.remove('highlight')
        }

        dropArea.addEventListener('drop', handleDrop, false)

        function handleDrop(e) {
            let dt = e.dataTransfer
            let files = dt.files

            handleFiles(files)
        }

        function handleFiles(files) {

            for (var i = 0; i < files.length; i++) {
                if(files[i].name == "history") {
                    manageFile(files[i])
                    break
                }
            }
        }

        function manageFile(file){
            var req = new XMLHttpRequest();

            var reader = new FileReader();

            reader.onload = function(e) {
                var text = reader.result;

                var lines = text.split('\\n');

                req.onreadystatechange = function()
                {
                    if(this.readyState == 4 && this.status == 200) {
                        document.getElementById('result').innerHTML = this.responseText;
                        document.getElementById('result').scrollIntoView()
                    } else {
                        document.getElementById('result').innerHTML = "Reading file...";
                    }
                }

                req.open('POST', '/', true);
                req.setRequestHeader('content-type', 'application/json');
                req.send(JSON.stringify(lines));
            }


            reader.readAsText(file)
        }
    </script>
</body>'''

db = SQLAlchemy(app)

if __name__ == "__main__":
    app.run()

