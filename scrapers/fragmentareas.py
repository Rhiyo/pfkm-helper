#print fragment areas

fragments = {'Artifact_BanditAmulet-shard':[], 'Artifact_SoulsCloak-shard':[],'Artifact_HermitKnightBracers-shard':[],
           'Artifact_BraveryRing-shard':[],'Artifact_StarGauntlet-shard':[],'Artifact_TigerArmor-shard':[],
           'Artifact_TrailblazerHelmet-shard':[]}

fragments_lost = {'Artifact_BanditAmulet-shard':{}, 'Artifact_SoulsCloak-shard':{},'Artifact_HermitKnightBracers-shard':{},
           'Artifact_BraveryRing-shard':{},'Artifact_StarGauntlet-shard':{},'Artifact_TigerArmor-shard':{},
           'Artifact_TrailblazerHelmet-shard':{}}

with open('history') as f:
    Lines = list(f)

currentArea = None
for i, line in enumerate(Lines): 
    line = line.split(" ")
    
    if(len(line) < 7):
        continue
    
    if line[3] == "[Area]" and line[6] == "loading":
        currentArea = line[4]
    
    if currentArea in ['CapitalThroneRoom','CapitalThroneRoom_Stone']:
        continue
    
    if line[4] in fragments.keys():
        if line[7] == 'lost\n':
            if(currentArea in fragments_lost[line[4]].keys()):
                fragments_lost[line[4]][currentArea] +=1
            else:
                fragments_lost[line[4]][currentArea] = 1
        elif line[7] == 'found\n':
            if currentArea in fragments_lost[line[4]].keys():
                if fragments_lost[line[4]][currentArea] > 0:
                     fragments_lost[line[4]][currentArea] -= 1
                else:
                    fragments[line[4]].append((i, currentArea))
            else:
                fragments[line[4]].append((i, currentArea))
            
            #else:
            #    fragments_lost[line[4]] -= 1
            
for k in fragments:
    print(k + " " + str(len(fragments[k])))
    for area in fragments[k]:
        print(area[1])
